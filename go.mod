module gitee.com/anyp2p/log

go 1.16

require (
	github.com/onsi/ginkgo v1.16.4
	github.com/onsi/gomega v1.15.0
	gopkg.in/yaml.v2 v2.4.0
)
